import type { EsbuildHtmlPluginOptions, CreateDocumentTreeOptions, LinkItem } from './../types/index.d'
import { Plugin } from 'esbuild'
import fs from 'fs'
import path from 'path'
import ejs from 'ejs'
import htmlminifier from 'html-minifier-terser'
import { parse } from 'node-html-parser'

async function createDocumentTree({
  template = `<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>esbuild-plugin-html</title>
  </head>
  <body>
    <div id="app"></div>
  </body>
  </html>`,
  links = [],
  preload = [],
  filename = 'index.html',
  minify = true,
  compile = true,
  renderData = {},
  publicPath = '/'
}: CreateDocumentTreeOptions) {
  // 判断模板类型
  const isFilePath = /\.[a-z0-9]+$/i.test(template)
  let templateStr = ''
  if (isFilePath) { // 文件模板
    const file = await fs.promises.readFile(path.resolve(process.cwd(), template))
    templateStr = file.toString()
  } else { // 字符串模板
    templateStr = template
  }
  // 是否需要压缩
  if (minify) {
    templateStr = await htmlminifier.minify(templateStr, Object.assign({
      collapseWhitespace: true,
      collapseInlineTagWhitespace: true,
      removeComments: true
    }, minify === true ? {} : minify))
  }
  // 是否需要编译ejs
  if (compile) {
    templateStr = ejs.render(templateStr, renderData)
  }
  // 模板字符串格式化为dom
  const root = parse(templateStr)
  const outputDir = path.dirname(filename)
  // 预加载文件
  preload.forEach(link => {
    switch (link.as) {
      case 'icon':
        root.querySelector('head')?.appendChild(parse(`<link rel="icon" type="${link.iconType}" href="${link.href}" />`))
        break
      case 'stylesheet':
        root.querySelector('head')?.appendChild(parse(`<link rel="stylesheet" href="${link.href}" />`))
        break
      case 'map':
        root.querySelector('head')?.appendChild(parse(`<link href="${link.href}" />`))
        break
      case 'script':
        root.querySelector('body')
          ?.appendChild(parse(`<script${link.enableESM ? ' type="module"' : ''} src="${link.href}"></script>`))
        break
    }
  })
  // 打包输出文件载入
  links.forEach(link => {
    const href = path.relative(outputDir, link.href)
    const fullPath = (`${publicPath}/${href}`).replace(/\/+/g, '/')
    switch (link.as) {
      case 'stylesheet':
        root.querySelector('head')?.appendChild(parse(`<link rel="stylesheet" href="${fullPath}" />`))
        break
      case 'map':
        root.querySelector('head')?.appendChild(parse(`<link href="${fullPath}" />`))
        break
      case 'script':
        root.querySelector('body')
          ?.appendChild(parse(`<script${link.enableESM ? ' type="module"' : ''} src="${fullPath}"></script>`))
        break
    }
  })
  // 反格式化为html字符串
  const html = root.innerHTML
  // 写入文件
  try {
    await fs.promises.opendir(outputDir)
  } catch (error) {
    await fs.promises.mkdir(outputDir, { recursive: true })
  } finally {
    await fs.promises.writeFile(filename, html)
  }
}



function esbuildHtmlPlugin(options: EsbuildHtmlPluginOptions = {}): Plugin {

  return {
    name: 'esbuild-plugin-html',
    setup(build) {
      build.initialOptions.metafile = true

      build.onEnd(async result => {
        const outputFiles = Object.keys(result.metafile?.outputs ?? {})
        const links: LinkItem[] = []
        let injectList: string[] = outputFiles

        if (Array.isArray(options.chunks) && options.chunks.length) {
          injectList = outputFiles.filter(href => {
            return !!options.chunks?.find(chunkName => path.basename(href).startsWith(chunkName))
          })
        }
        
        injectList.forEach(href => {
          if (/\.js$/.test(href)) {
            links.push({
              href,
              as: 'script',
              enableESM: build.initialOptions.format === 'esm'
            })
          } else if(/\.css$/.test(href)) {
            links.push({
              href,
              as: 'stylesheet'
            })
          } else if (/\.map$/.test(href)) {
            links.push({
              href,
              as: 'map'
            })
          }
        })

        await createDocumentTree(Object.assign(options, { links }))
      })
    }
  }
}

export default esbuildHtmlPlugin