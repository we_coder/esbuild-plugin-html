const { build } = require('esbuild')
const esbuildPluginHtml = require('../dist/index.js')
;(async () => {
  await build({
    entryPoints: ['./main.ts'],
    outdir: './output/static',
    format: 'esm',
    bundle: true,
    tsconfig: '../tsconfig.json',
    sourcemap: true,
    splitting: true,
    plugins: [
      esbuildPluginHtml.default({
        template: './index.html',
        minify: true,
        filename: './output/index.html',
        publicPath: './',
        chunks: ['main'],
        renderData: {
          title: 'title'
        }
      })
    ]
  })
})()